library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.NUMERIC_STD.all;

-- For the RAMs
Library xpm;
use xpm.vcomponents.all;

entity p2gt_deltaCondCheck is
  port(
    clk                   : in std_logic;
    phi1                  : in unsigned;
    phi2                  : in unsigned;
    cos_dphi_tmp          : out std_logic_vector(4 downto 0)
  );

end p2gt_deltaCondCheck;

architecture rtl of p2gt_deltaCondCheck is
  signal highPhi, lowPhi          : unsigned(11 downto 0);
  signal dPhi                     : unsigned(11 downto 0);
  signal dPhi_raw                 : std_logic_vector(10 downto 0) := (others => '0');
begin

  process(clk)
  begin
    if rising_edge(clk) then
      if phi1 > phi2 then
        highPhi <= phi1;
        lowPhi  <= phi2;
      else
        highPhi <= phi2;
        lowPhi  <= phi1;
      end if;
    end if;
  end process;

  dPhi <= highPhi - lowPhi;

  reg_distances : process(clk)
  begin
    if rising_edge(clk) then
      dPhi_raw <= std_logic_vector(dPhi(dPhi'high downto 1));
    end if;
  end process;

  -- xpm_memory_sprom: Single Port ROM
  cosdPhiMem : xpm_memory_sprom
    generic map (
      ADDR_WIDTH_A => 11, -- Size is a bit smaller than expected because we don't use the LSB of dPhi.
      AUTO_SLEEP_TIME => 0, -- DECIMAL
      ECC_MODE => "no_ecc", -- String
      MEMORY_INIT_FILE => "example.mem", -- String
      MEMORY_INIT_PARAM => "0", -- String
      MEMORY_OPTIMIZATION => "true", -- String
      MEMORY_PRIMITIVE => "auto", -- String
      MEMORY_SIZE => (2**11-1)*5, -- Size is a bit smaller than expected because we don't use the LSB of dPhi.
      MESSAGE_CONTROL => 0, -- DECIMAL
      READ_DATA_WIDTH_A => 5, -- DECIMAL
      READ_LATENCY_A => 1, -- DECIMAL
      READ_RESET_VALUE_A => "0", -- String
      USE_MEM_INIT => 1, -- DECIMAL
      WAKEUP_TIME => "disable_sleep" -- String
      )
      port map (
      dbiterra => open, -- 1-bit output: Leave open.
      douta => cos_dphi(4 downto 0), -- READ_DATA_WIDTH_A-bit output: Data output for port A read operations.
      sbiterra => open, -- 1-bit output: Leave open.
      addra => dPhi_raw, -- ADDR_WIDTH_A-bit input: Address for port A read operations.
      clka => clk, -- 1-bit input: Clock signal for port A.
      ena => '1', -- 1-bit input: Memory enable signal for port A. Must be high on clock
      -- cycles when read operations are initiated. Pipelined internally.
      injectdbiterra => '0', -- 1-bit input: Do not change from the provided value.
      injectsbiterra => '0', -- 1-bit input: Do not change from the provided value.
      regcea => '1', -- 1-bit input: Do not change from the provided value.
      rsta => '0', -- 1-bit input: Reset signal for the final port A output register
      -- stage. Synchronously resets output port douta to the value specified
      -- by parameter READ_RESET_VALUE_A.
      sleep => '0' -- 1-bit input: sleep signal to enable the dynamic power saving feature.
      );
  -- End of xpm_memory_sprom_inst instantiation

end rtl;

