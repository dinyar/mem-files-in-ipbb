library std;
use std.env.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use STD.TEXTIO.all;
use ieee.std_logic_textio.all;

entity testbench is
end testbench;

architecture behavior of testbench is

  constant half_period  : time      := 250000 ps;
  signal   clk          : std_logic := '1';

  signal phi1         : unsigned(11 downto 0);
  signal phi2         : unsigned(11 downto 0);
  signal cos_dphi : std_logic_vector(4 downto 0);

begin

  uut : entity p2gt_deltaCondCheck is
    port map (
      clk       : clk,
      phi1      : phi1,
      phi2      : phi2,
      cos_dphi  : cos_dphi
    );

  -- Clocks
  clk <= not clk after half_period;

  tb : process
    file FO    : text open write_mode is "results.txt";
    variable L : line;
  begin  -- process tb

    for i in 20 downto 0 loop
      phi1 <= to_unsigned(i, 12);
      phi2 <= to_unsigned(i+10, 12);

      wait for 2*half_period;

      cos_dphi_store <= cos_dphi;

      write(L, string'("phi1:"));
      write(L, i);
      write(L, string'(" phi2:"));
      write(L, i+10);
      write(L, string'(" result:"));
      hwrite(L, cos_dphi);
      writeline(FO, L);
    end for;

    finish(0);
  end process tb;

end;
