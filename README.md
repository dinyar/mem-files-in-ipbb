# Quick instructions

```bash
ipbb init mem-test
cd mem-test
ipbb add ssh://git@gitlab.cern.ch:7999/dinyar/mem-files-in-ipbb.git # Or KRB/HTTPS if that's being used

ipbb proj create sim sim-mems mem-files-in-ipbb:ipbb-sim-example top.d3
# Set up Vivado & Modelsim/Questasim
cd proj/sim-mems
ipbb sim setup-simlib
ipbb sim generate-project

./run_sim -c testbench work.glbl -do "run -all"
# Check in results.txt that the output is all 'XX'
ln -s ../../src/mem-files-in-ipbb/ipbb-example/firmware/hdl/example.mem .
./run_sim -c testbench work.glbl -do "run -all"
# result.txt contains non-XX values as output
```
